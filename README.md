# typeboost.css

**Typeboost.css** is a set of CSS rules to ensure readable, scalable and responsive typography. REMs are used for font sizes, paddings, and margins. Wrap your HTML content with class **.col** and you are done.

[![Typeboost.css](https://github.com/englishextra/typeboost.css/raw/master/img/typeboost.css-logo-1DB5FC-935x230.png)](https://github.com/englishextra/typeboost.css)

## npm Install

    npm install typeboost.css

## bower Install

    bower install typeboost.css

## Usage

	<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8" />
			<title></title>
			<link rel="stylesheet" href="typeboost.min.css" />
		</head>
		<body>
			<div class="col">
				<h1>Heading&#160;1</h1>
				<p>
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,
				</p>
			</div>
		</body>
	</html>

## Copyright

© [englishextra.github.com][], 2015

  [englishextra.github.com]: https://englishextra.github.com/
